import pygame
from pygame.locals import *

from OpenGL.GL import *
from OpenGL.GLU import *
import numpy as np

verticies = (
    (1,-1,-1),
    (1,1,-1),
    (-1,1,-1),
    (-1,-1,-1),
    (1,-1,1),
    (1,1,1),
    (-1,-1,1),
    (-1,1,1)
)

edges = (
    (0,1),
    (0,3),
    (0,4),
    (2,1),
    (2,2),
    (2,3),
    (2,7),
    (6,3),
    (6,4),
    (6,7),
    (5,1),
    (5,4),
    (5,7)
)

FloorVertices = (
    (-40,-1,-40),
    (40,-1,-40),
    (40,-1,40),
    (-40,-1,40)
    # (-4,-4,-1),
    # (4,-4,-1),
    # (-4,4,-1),
    # (-4,-4,-1)
)

FloorEdges = (
    (0,1),
    (1,3),
    (3,2),
    (2,0)
)


def Cube():
    glBegin(GL_LINES)
    for edge in edges:
        for vertex in edge:
            glColor3fv( (0.8,0,0.5) )
            glVertex3fv(verticies[vertex])
    glEnd()

def Floor():
    glBegin(GL_QUADS)
    for vertex in FloorVertices:
        glColor3fv( (0,0.8,0.5) )
        glVertex3fv(vertex)
    glEnd()

def printLine():
    print('Printing line')
    glLineWidth(5.0)
    glColor3fv( (1.0,1.0,1.0) )
    glBegin(GL_LINES)
    glVertex3f(1,1,1)
    glVertex3f(50,50,50)
    glEnd()

def TextSample(font):
    textString = "TextSample"
    textSurface = font.render(textString, True, (255,255,255,255),
                              (0,0,0,255))     
    textData = pygame.image.tostring(textSurface, "RGBA", True)     
    glRasterPos3d(0,0,0)
    glDrawPixels(textSurface.get_width(), textSurface.get_height(),
                 GL_RGBA, GL_UNSIGNED_BYTE, textData)

def toggleFloor(floorBool):
    floorBool = not floorBool
    print('Toggled floor, now display {}').format(str(floorBool))

def toggleCube(cubeBool):
    cubeBool = not cubeBool
    print('Toggled cube, now display {}').format(str(cubeBool))

def main():
    # Initiating pygame and pyopengl
    pygame.init()
    pygame.font.init()
    myfont = pygame.font.SysFont('Fira Code',20)
    size = width, height = 800,600
    screen = pygame.display.set_mode(size, DOUBLEBUF|OPENGL)


    # Setting up the scene
    CamPos = [0.0,1.0,0.0]
    CamOri = (0.0,0.0,1.0)
    gluPerspective(45,(800/600),0.1,50.0)
    glTranslatef(0.0, 0.0, -10.0)
    glRotatef(25,2,1,0)
    cubeExists = True
    floorExists = True
    lineExists = False
    while True:
        for event in pygame.event.get():
            if event.type == pygame.QUIT:
                pygame.quit()
                quit()
            if event.type == pygame.MOUSEBUTTONDOWN:
                # if pygame.mouse.get_pressed()[0] == True:
                #     print('mouse pressed')
                #     cubeExists = False
                lineExists = not lineExists
            if event.type == pygame.KEYDOWN:
                if event.key == pygame.K_LEFT:
                    glTranslatef(0.3,0,0)
                if event.key == pygame.K_RIGHT:
                    glTranslatef(-0.3,0,0)
                if event.key == pygame.K_UP:
                    glTranslatef(0,-0.3,0)
                if event.key == pygame.K_DOWN:
                    glTranslatef(0,0.3,0)

                # Handling movement
                # Also need to keep track of where we are looking at
                if event.key == pygame.K_z:
                    move_mat = [0.0,0.0,0.3]
                    CamPos = np.multiply(move_mat,CamPos)
                    CamPos = np.multiply(CamPos,CamOri)
                    # glTranslatef(0,0,0.3)
                    glTranslatef(*CamPos)
                if event.key == pygame.K_s:
                    glTranslatef(0,0,-0.3)
                if event.key == pygame.K_q:
                    glRotatef(-0.3,0,1,0)
                if event.key == pygame.K_d:
                    glRotatef(0.3,0,1,0)

                # Other keybindings
                if event.key == pygame.K_f:
                    # toggleFloor(floorExists)
                    floorExists = not floorExists
                if event.key == pygame.K_c:
                    # toggleCube(cubeExists)
                    cubeExists = not cubeExists 
                if event.key == pygame.K_ESCAPE:
                    pygame.quit()
                    quit()
        # glRotatef(1.0,1.0,1.0,1.0)
        glClear(GL_COLOR_BUFFER_BIT|GL_DEPTH_BUFFER_BIT)
        if floorExists == True:
            Floor()
        if cubeExists == True:
            Cube()
        if lineExists == True:
            printLine()
        TextSample(myfont)
        pygame.display.flip()
        pygame.time.wait(10)

if __name__=='__main__':
    main()
